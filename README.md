# Introduction

As the exercise presented ask for a backend, one has been made with Django.

All requirements can be found inside requirements.txt

Multiple security improvements and bug fix needed before production.


Database is created easily with Django (by default sqlite3) but is easy to 
implement PostgreSQL or MariaDB (MySQL) 

Manually creation is possible with the following commands 
(tested MariaDB Ver 15.1 D):

```
CREATE DATABASE carritocompra;
USE carritocompra;
CREATE TABLE item (id integer NOT NULL UNIQUE PRIMARY KEY, 
value FLOAT, volume FLOAT);
CREATE TABLE cart (id integer NOT NULL UNIQUE PRIMARY KEY, 
volume FLOAT);
CREATE TABLE item_cart (id integer NOT NULL UNIQUE PRIMARY KEY, 
item_id integer NOT NULL, cart_id integer NOT NULL ); 

```

An scheme of db:


![Scheme](https://gitlab.com/testknapsack/django-backend/raw/master/doc/scheme.png)


# Installation - Linux

1º Clone repository

```
git clone https://gitlab.com/testknapsack/django-backend
```
2º Install python virtualenv


```
sudo apt install python3-virtualenv
```

3º Create new Virtualenv
```
python3 -m venv /path/to/venv

```

4º Activate Virtualenv and install requirements
```
cd django-backend
source /path/to/venv/bin/activate
pip3 install -r requirements.txt
```

5º Run Django Server
´´´
python3 manage.py migrate //Creates a development sqlite3 db
python3 manage.py runserver

´´´
