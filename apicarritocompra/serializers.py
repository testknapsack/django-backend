from rest_framework.serializers import ModelSerializer

from apicarritocompra.models import Item, Cart


class ItemSerializer(ModelSerializer):
    class Meta:
        model = Item
        fields = '__all__'


class CartSerializer(ModelSerializer):
    class Meta:
        model = Cart
        fields = '__all__'

