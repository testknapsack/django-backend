from django.db import models


class Item(models.Model):
    value = models.FloatField()
    volume = models.FloatField()


class Cart(models.Model):
    item = models.ManyToManyField(to=Item)
    volume = models.FloatField(default=0.0)

