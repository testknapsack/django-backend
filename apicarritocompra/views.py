from rest_framework.generics import CreateAPIView, UpdateAPIView, DestroyAPIView, ListAPIView

from apicarritocompra.models import Cart, Item
from apicarritocompra.serializers import CartSerializer, ItemSerializer


class IndexView(ListAPIView):
    queryset = Item.objects.all()
    serializer_class = ItemSerializer


class Create(CreateAPIView):
    queryset = Cart.objects.all()
    serializer_class = CartSerializer


class Edit(UpdateAPIView):
    queryset = Cart.objects.all()
    serializer_class = CartSerializer


class Delete(DestroyAPIView):
    queryset = Cart.objects.all()
    serializer_class = CartSerializer
